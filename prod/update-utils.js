function getRangeListIndices(range) {
  let currentRow = range.getRow();
  let lastRow = range.getLastRow()
  let length = (lastRow - currentRow + 1);
  let arrIndices = Array.from({ length, }, (_, i) => i + currentRow)
  return currentRow === lastRow ? currentRow : arrIndices
}

function copyValuesFrom(fromSheet, fromParams, _Browser, fromConditionalcb) {
  const {body: fromBody, headerObj: fromHeaderObj} = getDataBySheet({sheet: fromSheet, headerIdx: fromParams.headerRowIdx ,bodyRowStart: fromParams.dataStartRow });
  const activeRangeListOfRowIdx = fromParams.activeRangeList.getRanges()
  .flatMap(getRangeListIndices)
  .sort((a,b) => a-b)
  const convertIndexToBody = (num) => fromBody[num- (fromParams.dataStartRow+1)]
  const selectedList = activeRangeListOfRowIdx.map(convertIndexToBody)
  const isUserAcceptPrompt = fromConditionalcb(selectedList);
  if (!isUserAcceptPrompt) {
    return (...args) => null;
  }

  return function moveValuesTo(toSheet, toParams, toConditionalcb) {
    if (!toConditionalcb) {
      throw (`${fromConditionalcb.errorMsg}`)
    }

    const {body: toBody, headerObj: toHeaderObj, headers: toHeaders} = getDataBySheet({sheet: toSheet, headerIdx: toParams.headerRowIdx ,bodyRowStart: toParams.dataStartRow }); 

    const rowAsToSheetFormat = selectedList.map(props => {
      debugger
      return toHeaders.map(str => props.hasOwnProperty(str) ? props[str] : "")
    })
    rowAsToSheetFormat.forEach( values => toSheet.appendRow(values))
  }
}
