

function copyValuesFromShortlist(fromSheet, fromParams, _Browser, fromConditionalcb) {
  const { body: fromBody, headerObj: fromHeaderObj } = getDataBySheet({ sheet: fromSheet, headerIdx: fromParams.headerRowIdx, bodyRowStart: fromParams.dataStartRow });
  const activeRangeListOfRowIdx = fromParams.activeRangeList.getRanges()
    .flatMap(getRangeListIndices)
    .sort((a, b) => a - b)
  const convertIndexToBody = (num) => fromBody[num - (fromParams.dataStartRow + 1)]
  const selectedList = activeRangeListOfRowIdx.map(convertIndexToBody)
  // const isUserAcceptPrompt = fromConditionalcb(selectedList);
  // if (!isUserAcceptPrompt) {
  //   return (...args) => null;
  // }

  return function moveValuesTo(toSheet, toParams, toConditionalcb) {
    // if (!toConditionalcb) {
    //   throw (`${fromConditionalcb.errorMsg}`)
    // }
    let rowIdx = _Browser.inputBox("Which row would you like these participants moved to?")
    let num = Number(rowIdx)
    let inputValidations = [
      { fail: (isNaN(num)), msg: 'Unable to determine number, please try again' },
      { fail: (num <= 5), msg: 'Unable to move to row below 5' },
      { fail: (num > toSheet.getLastRow()), msg: `Unable to move to row above ${toSheet.getLastRow()} 5` },
    ]
    debugger
    if (inputValidations.find(({ fail }) => fail === true)) {
      let { msg } = inputValidations.find(({ fail }) => fail === true)
      _Browser.msgBox(msg)
      return
    }

    Logger.log(`num: ${num}`)
    const { body: toBody, headerObj: toHeaderObj, headers: toHeaders } = getDataBySheet({ sheet: toSheet, headerIdx: toParams.headerRowIdx, bodyRowStart: toParams.dataStartRow });

    const setToSchedule = (props) => () => props.getRangeByColHeader("Participant Status").setValue("Scheduled")
    const rowAsToSheetFormat = selectedList.map((props, rowIdx) => {
      const getValueFromRangeByColIdx = (col) => toSheet.getRange(num + rowIdx, col, 1, col + 1).getValue()
      let rowAsValues = toHeaders.map((str, idx) => {
        Logger.log(`"${typeof str}"`)
        if (idx === 0) {
          return ""
        }

        str = typeof str === "string" ? str.trim() : str
        let getExisitingValue = getValueFromRangeByColIdx(idx + 1)
        let headerOfPropValue = props.hasOwnProperty(str) ? props[str.trim()] : getExisitingValue;
        headerOfPropValue = headerOfPropValue[0] === "+" ? "'" + headerOfPropValue : headerOfPropValue
        return headerOfPropValue
      })

      Object.defineProperty(rowAsValues, 'setSchedule', {
        value: setToSchedule(props),
        enumerable: false,
        writable: false,
        configurable: false
      })
      return rowAsValues
    })

    const numOfParticipants = rowAsToSheetFormat.length
    const numCols = rowAsToSheetFormat[0].length
    let updateRange = toSheet.getRange(num, 1, numOfParticipants, toSheet.getLastColumn())
    updateRange.setValues(rowAsToSheetFormat)
    rowAsToSheetFormat.forEach(props => props.setSchedule())
  }
}

function test_moveToScheduleFromShortlist() {
  let spreadsheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1qATEudMqPQI0Yn5IdPfjryXGL1OJVbPXOsGAqKmLBsI/edit#gid=1823584743")
  const sheetShortList = spreadsheet.getSheets().find(props => props.getName() === "Shortlist")
  const rangeList = sheetShortList.getRangeList(['D15']);
  spreadsheet.setActiveSheet(sheetShortList)
  spreadsheet.setActiveRange(sheetShortList.getRange("D21"))
  // spreadsheet = spreadsheet.setActiveRange(rangeList)

  let _Browser = { msgBox: (str) => Logger.log(str), inputBox: () => 5 }
  // moveToShortListFromResponses(spreadsheet, _Browser, sheet)
  moveToScheduleFromShortlist(spreadsheet, _Browser)
}


function moveToScheduleFromShortlist(spreadsheet, _Browser) {
  const sheet = spreadsheet.getActiveSheet()
  // const confirmAction = STATIC_ORION.ConfirmAction("Move Data From ShortList to Schedule", "Legal Name", _Browser)
  // confirmAction.errorMsg = 'Failed to meet conditionals to pass data to ShortList'
  const activeRangeList = sheet.getActiveRangeList();
  const hasActiveRangeSelection = (range) => range !== null ? range.getRow() > STATIC_ORION.Sheets.ShortList.dataStartRow ? true : false : false
  hasActiveRangeSelection.errorMsg = `Range Selection includes below row #${STATIC_ORION.Sheets.ShortList.dataStartRow} which isn't valid, please adjust and re-run`
  const isActiveSheetShortList = (range) => range.getSheet().getName() === STATIC_ORION.Sheets.ShortList.name
  isActiveSheetShortList.errorMsg = `Range Selection for this action is limited to ${STATIC_ORION.Sheets.ShortList.name}`
  const isValidSelectionParams = [hasActiveRangeSelection, isActiveSheetShortList]
  const isValidSelection = (activeRange) => isValidSelectionParams.every(fn => fn(activeRange))
  // const shortListSheet = sheet.getSheetByName(STATIC_ORION.Sheets.ShortList.name)
  const shortListParams = Object.assign({}, STATIC_ORION.Sheets.ShortList, { activeRangeList })
  const scheduleSheet = sheet.getParent().getSheetByName(STATIC_ORION.Sheets.Schedule.name)
  const scheduleParams = Object.assign({}, STATIC_ORION.Sheets.Schedule);
  let pass = !isValidSelection(sheet.getActiveRange())
  let row = sheet.getActiveRange().getRow();
  if (!isValidSelection(sheet.getActiveRange())) {
    let errorMsg = isValidSelectionParams.filter(fn => !fn(sheet.getActiveRange()))
      .map(fn => fn.errorMsg)
    _Browser.msgBox(`${errorMsg.join('\\n')}`)
    debugger
    return
  }
  // let hasScheduleTabFn = () => sheet.getParent().getSheetByName(STATIC_ORION.Sheets.Schedule.name)
  // hasScheduleTabFn.errorMsg = `Unable to find Sheet Name: ${STATIC_ORION.Sheets.Schedule.name}`
  const moveValuesToSchedule = copyValuesFromShortlist(sheet, shortListParams, _Browser)
  moveValuesToSchedule(scheduleSheet, scheduleParams);

  pass && _Browser.msgBox(`The selected list have been appended to the "Schedule" Tab  \\n At the bottom`)

  function hasValidMoveCriterias(range) {
    return isValidSelection(range)
  }
}

