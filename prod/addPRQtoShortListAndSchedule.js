function applyResponsesHeadersToShortlistSchedule(spreadsheet){
  try{
    addPRQtoShortListAndSchedule(spreadsheet)
  } catch(e){
    Logger.log(e)
  }
}
function test(){
  let sheets = ["Schedule", "Shortlist"]
  const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  sheets.forEach(anon)

  function anon(str){
    let sheet = spreadsheet.getSheetByName(str);
    let [values] = sheet.getRange(5, 1, 1, sheet.getLastColumn()).getValues()
    debugger
    let idx = values.lastIndexOf("Timezone") + 1
    debugger
  }
}
function addPRQtoShortListAndSchedule(spreadsheet){
  spreadsheet = spreadsheet || SpreadsheetApp.openById("1RVHciqjyTIR6tETrbq5ubf0v1zFIUHbObOFllZQ-kK4")
  // const sheets = spreadsheet.getSheets()
  const selectionList = ["Shortlist", "Schedule"]
  const selectSheets = selectionList.map( str => spreadsheet.getSheetByName(str))
  let getIdxAfterTmzBySheet = ( sheet ) => {
      let [values] = sheet.getRange(5,1,1,sheet.getLastColumn()).getValues();
      let idx = values.lastIndexOf("Timezone")
      return (idx === -1 ? values.length : idx) +2
    }
  const sheets = spreadsheet.getSheets()
  const responseSheets = sheets.filter( sheet => (sheet.getName().toLowerCase().includes("responses") && !!sheet.getFormUrl()) );
  const responseHeaders = responseSheets.map(getSheetHeaderValues)
                                        .reduce( (acc, next) => acc.concat(...next),[]);
                                        
  // const prHeaders = prSheet.getRange(1,1, 1, prSheet.getLastColumn())
  // let [prValues] = prHeaders.getValues();
  selectSheets.forEach( sheet => {
    let name = sheet.getName()
    let col = getIdxAfterTmzBySheet(sheet)
    let [currentHeader] = sheet.getRange(5,1,1,col).getValues();
    let [outSideMainHeaders] = sheet.getRange(5,col, 1, sheet.getLastColumn())
                                    .getValues()
                                    // .filter(str => str !== "")
    
    if(outSideMainHeaders.join("") !== ""){
      Logger.log(`${name}: Headers from Responses Found fn:addPRQtoShortListAndSchedule1`)
      return 
    }
    let appendingHeaderValues = responseHeaders.filter(value => !currentHeader.includes(value))
    let headerRange = sheet.getRange(5, col, 1, appendingHeaderValues.length)
    headerRange.setValues([appendingHeaderValues])
  })


  function getResponsesSheets(spreadsheet){
    let sheets = spreadsheet.getSheets()
    let responsesSheets = sheet.filter(sheet => {
      let sheetName = sheet.getName().toLowerCase()
      return sheetName.includes("responses") && !!sheet.getFormUrl()
    })
    return responsesSheets
  }
  function getSheetHeaderValues(sheet, idx, arr) {
    let headerRange = sheet.getRange(1,1,1, sheet.getLastColumn())
    let [values] = headerRange.getValues()
    return arr.length > 1 ? values.map( getHeaderValWithFormName(sheet) ) : values
  }

}