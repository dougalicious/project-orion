
function copyValuesFromResponses(fromSheet, fromParams, _Browser) {
  const { body: fromBody, headerObj: fromHeaderObj } = getDataBySheet({ sheet: fromSheet, headerIdx: fromParams.headerRowIdx, bodyRowStart: fromParams.dataStartRow });
  const activeRangeListOfRowIdx = fromParams.activeRangeList.getRanges()
    .flatMap(getRangeListIndices)
    .sort((a, b) => a - b)
  const convertIndexToBody = (num) => fromBody[num - (fromParams.dataStartRow + 1)]
  const selectedList = activeRangeListOfRowIdx.map(convertIndexToBody)
  // const isUserAcceptPrompt = fromConditionalcb(selectedList);
  // if (!isUserAcceptPrompt) {
  //   return (...args) => null;
  // }

  return function moveValuesTo(toSheet, toParams) {
    
     const _hasMultipleForms = hasMultipleForms(toSheet)
    const fromHeaders = Object.keys(fromHeaderObj) 
    const { body: toBody, headerObj: toHeaderObj, headers: toHeaders } = getDataBySheet({ sheet: toSheet, headerIdx: toParams.headerRowIdx, bodyRowStart: toParams.dataStartRow });

    const rowAsToSheetFormat = selectedList.map(props => {

      return toHeaders.map((str, idx) => {
        const idxInArr = (arr, idx) => arr.includes(idx);
        const strInHeaderList = (arr, str) => arr.includes(str)
        const isEmptyValueColum = strInHeaderList(toParams.defaultHeadersAsString, str)
        const isPrimaryKeyValues = strInHeaderList(toParams.primaryHeadersAsString, str);
       let valueFromPrev = () =>  {
          let strHasReturn =  str.indexOf('\n') !== -1
          str = _hasMultipleForms && strHasReturn ? str.slice(0, str.indexOf('\n')-1 ) : str
          debugger
          return props.hasOwnProperty(str.trim()) ? props[str.trim()] : ""
          }
        let pptStatusDefault = "Pending Approval"
        return idx === 0 ? pptStatusDefault : isEmptyValueColum ? "" : isPrimaryKeyValues ? findKeyInQuestion(props, str) : valueFromPrev();

        function findKeyInQuestion(props, str) {
          let headerMatch = fromHeaders.find(fromHeader => new RegExp(str, "gi").test(fromHeader));
          let headerOfPropValue = props.hasOwnProperty(headerMatch) ? props[headerMatch] : ""
          // Logger.log(`headermatch: ${headerMatch} value: "${headerOfPropValue}"`)
          headerOfPropValue = headerOfPropValue[0] === "+" ? "'" + headerOfPropValue : headerOfPropValue
          return headerOfPropValue
        }
      })

    })
    rowAsToSheetFormat.forEach(values => toSheet.appendRow(values))
    applyBackgroundByRow(fromSheet, activeRangeListOfRowIdx)

    function applyBackgroundByRow(sheet, activeRangeListOfRowIdx){
      activeRangeListOfRowIdx.forEach( rowIdx => {
        const range = sheet.getRange(rowIdx, 1, 1, sheet.getLastColumn())
        range.setBackground("#d9ead3")
      })
    }
  }
}

// to test on specific ss id
function test_moveToShortListFromResponses() {
  let spreadsheet = SpreadsheetApp.openById("1BTuhfKLgcLzSxJ35Q2vgNANtie0FcBM8XxGqF7Lqq30")
  const sheet = spreadsheet.getSheets().find(props => props.getName() === "Participant Responses")
  const rangeList = sheet.getRangeList(['D15']);
  spreadsheet.setActiveSheet(sheet)
  spreadsheet.setActiveRange(sheet.getRange("D30"))
  // spreadsheet = spreadsheet.setActiveRange(rangeList)

  let _Browser = { msgBox: (str) => Logger.log(str) }
  // moveToShortListFromResponses(spreadsheet, _Browser, sheet)
  moveToShortListFromResponses(spreadsheet, _Browser, sheet)
}


function moveToShortListFromResponses(spreadsheet, _Browser) {
  spreadsheet = spreadsheet || SpreadsheetApp.getActiveSpreadsheet();
  _Browser = _Browser || Browser;
  applyResponsesHeadersToShortlistSchedule(spreadsheet)
  // Logger.log(spreadsheet.getName())
  //DEV
  // const sheet = spreadsheet.getSheetByName("Participant Responses")
  // prod
  const sheet = spreadsheet.getActiveSheet() 
  //5.23 removing confirmation
  // const confirmAction = STATIC_ORION.ConfirmAction("Move Data From 'Responses' to 'ShortList'", "getRowIdx", _Browser)
  // confirmAction.errorMsg = 'Failed to meet conditionals to pass data to ShortList'
  //DEV
  const activeRangeList = sheet.getActiveRangeList();
  // const rangeList = sheet.getRangeList(["D30", "B3:C9"])
  // const activeRangeList = sheet.setActiveRangeList(rangeList)
  /*** validation check */
  const hasActiveRangeSelection = (range) => range !== null ? range.getRow() > STATIC_ORION.Sheets["Participant Responses"].dataStartRow ? true : false : false
  hasActiveRangeSelection.errorMsg = `Range Selection includes below row #${STATIC_ORION.Sheets["Participant Responses"].dataStartRow} which isn't valid, please adjust and re-run`
  const isActiveSheetParticipantResponses = (range) => range.getSheet().getName().toLowerCase().includes("responses")
  isActiveSheetParticipantResponses.errorMsg = `Range Selection for this action is limited to form Sheet name including word 'Responses'`
  const isSheetResponse = (range) => !!range.getSheet().getFormUrl();
  isSheetResponse.errorMsg = "Only works with Sheets with a Form"
  const hasShortListSheet = (range) => range.getSheet().getParent().getSheets().find(sheet => sheet.getName() === "Shortlist");
  hasShortListSheet.errorMsg = "Unable to find 'Shortlist' Sheet"
  const isTimestampHeaderRow = (range) => range.getSheet().getRange(1, 1, 1, range.getSheet().getLastColumn()).getValues()[0].includes("Timestamp");
  isTimestampHeaderRow.errorMsg = `Please ensure there's "Timestamp" Header in the header, and the header is row #1`
  

  /*** end validation check */
  const isValidSelectionParams = [hasActiveRangeSelection, isActiveSheetParticipantResponses, isSheetResponse, hasShortListSheet, isTimestampHeaderRow]
  const isValidSelection = (activeRange) => isValidSelectionParams.every(fn => fn(activeRange))
  // const shortListSheet = sheet.getSheetByName(STATIC_ORION.Sheets.ShortList.name)
  const responseParams = Object.assign({}, STATIC_ORION.Sheets["Participant Responses"], { activeRangeList })
  const shortListSheet = sheet.getParent().getSheetByName(STATIC_ORION.Sheets.ShortList.name)
  const shortListParams = Object.assign({}, STATIC_ORION.Sheets.ShortList);
  let pass = !isValidSelection(sheet.getActiveRange())
  let row = sheet.getActiveRange().getRow();
  if (!isValidSelection(sheet.getActiveRange())) {
    let errorMsg = isValidSelectionParams.filter(fn => !fn(sheet.getActiveRange()))
      .map(fn => fn.errorMsg)
    _Browser.msgBox(`${errorMsg.join('\\n')}`)
    return
  }
  let hasResponsesTabFn = () => sheet.getParent().getSheetByName(STATIC_ORION.Sheets["Participant Responses"].name)
  hasResponsesTabFn.errorMsg = `Unable to find Sheet Name: ${STATIC_ORION.Sheets["Participant Responses"].name}`
  
  const moveValuesToShortlist = copyValuesFromResponses(sheet, responseParams, _Browser)
  moveValuesToShortlist(shortListSheet, shortListParams, hasResponsesTabFn);
  
  pass && _Browser.msgBox(`The selected list have been appended to the "Shortlist" Tab  \\n At the bottom`)
}



