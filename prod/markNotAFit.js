function markNotAFit(spreadsheet, _Browser) {
  _Browser = _Browser || { msgBox: (str) => Logger.log(str) }
  // let sheetName = "Participant Responses"
  spreadsheet = spreadsheet //|| SpreadsheetApp.openById("1qATEudMqPQI0Yn5IdPfjryXGL1OJVbPXOsGAqKmLBsI");
  let sheet = spreadsheet.getActiveSheet()
  let activeRange = sheet.getActiveRange();
  /** VALIDATION CHECKs */
  // const confirmAction = STATIC_ORION.ConfirmAction("Mark Rows as Not Fit", null, _Browser)
  // confirmAction.errorMsg = 'Failed to meet code conditionals to pass data to mark not fit'
  const hasActiveRangeSelection = (range) => range !== null ? range.getRow() > 1 ? true : false : false
  hasActiveRangeSelection.errorMsg = `Range Selection includes below row #${activeRange.getRow()} which isn't valid, please adjust and re-run`
  const isActiveSheetShortList = (range) => range.getSheet().getName().toLowerCase().includes("responses") 
  const isFormSheet =(range) => range.getSheet().getFormUrl();
  isFormSheet.errorMsg = `Range Selection for this action is limited to a FORM associated with the Sheet`
  isActiveSheetShortList.errorMsg = `Range Selection for this action is limited to "Responses" associated with Sheet Name`
  const isValidSelectionParams = [hasActiveRangeSelection, isActiveSheetShortList, isFormSheet]
  const isValidSelection = (activeRange) => isValidSelectionParams.every(fn => fn(activeRange))

  Logger.log(`activeRange: ${activeRange.getA1Notation()}`)
  if (!isValidSelection(activeRange)) {
    let errorMsg = isValidSelectionParams.filter(fn => !fn(sheet.getActiveRange()))
      .map(fn => fn.errorMsg)
    _Browser.msgBox(`${errorMsg.join('\\n')}`)
    return
  }
  let activeRangeList = spreadsheet.getActiveRangeList();
  
  let activeRangeListOfRowIdx = activeRangeList.getRanges().flatMap(getRangeListIndices).sort((a,b) => a-b)
  debugger
  // const isUserAcceptPrompt = confirmAction(activeRangeListOfRowIdx.map(num =>  `row ${num}`));
  // if (!isUserAcceptPrompt) {
  //   return (...args) => null;
  // }
  activeRange = sheet.getActiveRange().getValues();
  const setBackgroundGrey = (idx) => sheet.getRange(idx, 1, 1, sheet.getLastColumn()).setBackground("#b7b7b7");
  activeRangeListOfRowIdx.map(setBackgroundGrey)
  Logger.log(`mark Not Fit complete`)
}

