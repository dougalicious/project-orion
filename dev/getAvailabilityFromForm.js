function getAvailabilityFromForm(activeSpreadsheet) {
  const id = "1svFwE_HdmyWKB8W0GEqcWh7ZpTF7trG7NKTmCBk-X70"
  activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.getActiveSpreadsheet()
  const getSheetByName = (str) => {
    try{
      return activeSpreadsheet.getSheetByName(str)
      }catch(e){
        throw(`unable to find ${str}: ${e}`)
      }
  }
  const scheduleSheet = getSheetByName("Schedule")

  fetchTimeAvailablity(activeSpreadsheet)

  function fetchTimeAvailablity(activeSpreadsheet) {
    const fetchParticipantResponseSheet = (ss) => {
      try{
        return ss.getSheetByName("Participant Response")
      } catch(e){
        throw(`Unable to find "Participant Responses" sheet : ${e}`)
      }
    }

    const sheet = fetchParticipantResponseSheet(activeSpreadsheet)
    const availableTimes = getFormData(activeSpreadsheet)

    debugger

    function getFormData(activeSpreadsheet) {
      // const anon = getFormUrl(activeSpreadsheet)
      let fetchCommaSpaceIdx = (str) => str.indexOf(', ') +2
      let fetchEndOfDateStrIdx = (str) => str.indexOf(" from") !== -1 ? str.indexOf(" from") : str.indexOf(" -")
       let fetchEndOfDateStrLastIdx = (str) => str.lastIndexOf(" from") !== -1 ? str.lastIndexOf(" from")+6 : str.lastIndexOf(" -")+3
  
      const availableTimes = compose( getCheckBoxChoices, getFormItems, getFormUrl)(activeSpreadsheet);
      const availableTimesAsObj = availableTimes.map(asDateTimeCustomObj).filter(props => !!props.dateObj)
      debugger
      const {headerObj, getBody} = getDataBySheet(scheduleSheet, 3, 4)
      

      function populateScheduleSheet(props, idx, arr){
        // const getNextRange = ()
         let rangeLocation = {row: idx+5, col: headerObj["Session Details"]}
         let range = scheduleSheet.getRange(rangeLocation.row, rangeLocation.col)
         
  

        function doesDateExist(){

        }
        function populateDate(range, {month, day, year}){
          range.setValue(`${month}/${day}/${year}`)
          .setNumberFormat("mmmm d \[dddd\]")
          .setFontWeight("BOLD");
          scheduleSheet.getRange(range.getRow(),1,1,scheduleSheet.getLastColumn()).setBackground("#9fc5e8");
        }
        function populateTimeRange(){

        }
      }
      
      function asDateTimeCustomObj(str, idx){
        let endOfDateIdx = fetchEndOfDateStrIdx(str);
        let commaSpaceIdx = fetchCommaSpaceIdx(str)
        let endOfDateLastIdx = fetchEndOfDateStrLastIdx(str);
        let isValidDate = [endOfDateIdx, commaSpaceIdx].every(num => num !== -1)
        const dateAsString =  isValidDate ? `${str.slice(commaSpaceIdx, endOfDateIdx)}, ${new Date().getFullYear()}` : null
        if(idx === 10){
          debugger
        }
        const date = isValidDate ? new Date(dateAsString) : null
        const dateObj = isValidDate ? getddMMYYY(date) : null
        const timeRange = str.slice(endOfDateLastIdx)

        return {
          dateObj,
          timeRange,
          asStr: str
        }
             function getddMMYYY(date) {
          try{
          return{
            month: date.getMonth()+1,
            year: date.getFullYear(),
            day: date.getDate()
          }
          } catch(e){
            return null
          }
        }
      }
      /*
      convert each row into {date, time, other}
      "Monday, December 7 - 10:00-11:00am EST"
      let commaSpaceIdx = str.indexOf(', ')
      let endOfDate = str.
      let date = str.slice()
      { 
        date: 12/7/2021
        timeRange: 10:00-11:00am
        str: "Monday, December 7 - 10:00-11:00am EST"
        }

      */


      let filterOutNonDates = (str) => [",", "-"].every( char => str.includes(char))
      let filterDate = (str) => str.slice(str.indexOf(",")+1, str.indexOf('-')).trim();
      let addYear = (str) => `${str}, ${(new Date).getFullYear()}`

      Logger.log(availableTimes.map( str => `"${str}"`))
      const fetchDate = (str) => {

      }
      const values = availableTimes.map(prop => prop.getValues())

    }
    function getFormUrl(sheet) {
      try {
        let formUrl = sheet.getFormUrl();
        let form = FormApp.openByUrl(formUrl);
        return form
      } catch (e) {
        throw (`Unable to find form associated to Sheet ${sheet.getName()}, ${e}`)
      }
    }
    function getCheckBoxChoices(formItems) {
      return formItems.flatMap(formItem => formItem.asCheckboxItem().getChoices().flatMap(prop => prop.getValue()))
        .filter(onlyUnique)
    }
    function getFormItems(form) {

      try {
        const hasTimeAvailablity = (formItem) => formItem.getTitle().toLowerCase().trim().includes("*time availability")
        const hasCheckBox = (formItem) => formItem.getType() == FormApp.ItemType.CHECKBOX
        const hasTimeAndCheckBox = (formItem, idx) => {
          let title = formItem.getTitle()
          if (idx === 24) {
            let one = hasTimeAvailablity(formItem)
            let two = hasCheckBox(formItem)
            // debugger
          }

          return hasTimeAvailablity(formItem) && hasCheckBox(formItem)
        }
        let formItems = form.getItems();
        // let anon = formItems.map( prop => {return {title: prop.getTitle(), desc: prop.getHelpText(), type: prop.getType() == FormApp.ItemType.CHECKBOX}})
        // debugger
        let formIemsWithTimeAvailability = formItems.filter(hasTimeAndCheckBox)
        return formIemsWithTimeAvailability
      } catch (e) {
        throw (`Unable to find form Items with "*Time Availability" notation, ${e}`)
      }
    }
    function tryCatchWrapper(errorMessage, source, method) {
      try {
        return source[method]()
      } catch (e) {
        throw (`${errorMessage}: ${e}`)
      }
    }
  }
}