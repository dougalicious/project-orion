function moveToScheduleSheet(spreadsheet, _Browser) {
  const validationList = [isTabSelection("Shortlist"), isRangeBelow(5), hasNewScheduleFormat]
  const isValidSelection = isAllFnValid(validationList, spreadsheet)
  // isValidSelection(spreadsheet)

  const getShortList = (spreadsheet)

  
  function addPropertiesToSchedule(shortListProps, scheduleProps){

  }
  function getShortListProperties(spreadsheet){

  }
  function getScheduleProperties(spreadsheet){

  }
  function isAllFnValid(arr, ss){
    return arr.reduce( (acc, fn) => acc === true ? fn(ss) : false, true)
  }

  function prompt(msg, error){
    Browser.msgBox(msg)
    if(!!error){
      throw(error)
    } 
  }
  function isTabSelection(str){
    return function(ss){
      const activeSheet = ss.getActiveSheet();
      const activeSheetName = activeSheet.getName()
      const hasStrRegex = new RegExp(str,'gi')
      return hasStrRegex.test(activeSheetName)
    }
  }
}
function moveToScheduleFromShortlist(spreadsheet, _Browser) {
  const sheet = spreadsheet.getActiveSheet()
  const activeRangeList = sheet.getActiveRangeList();
  /** Criteria for a valid selection */
  const hasActiveRangeSelection = (range) => range !== null ? range.getRow() > STATIC_ORION.Sheets.ShortList.dataStartRow ? true : false : false
  hasActiveRangeSelection.errorMsg = `Range Selection includes below row #${STATIC_ORION.Sheets.ShortList.dataStartRow} which isn't valid, please adjust and re-run`
  const isActiveSheetShortList = (range) => range.getSheet().getName() === STATIC_ORION.Sheets.ShortList.name
  isActiveSheetShortList.errorMsg = `Range Selection for this action is limited to ${STATIC_ORION.Sheets.ShortList.name}`
  const isValidSelectionParams = [hasActiveRangeSelection, isActiveSheetShortList]
  const isValidSelection = (activeRange) => isValidSelectionParams.every(fn => fn(activeRange))
  // const shortListSheet = sheet.getSheetByName(STATIC_ORION.Sheets.ShortList.name)
  const shortListParams = Object.assign({}, STATIC_ORION.Sheets.ShortList, { activeRangeList })
  const scheduleSheet = sheet.getParent().getSheetByName(STATIC_ORION.Sheets.Schedule.name)
  const scheduleParams = Object.assign({}, STATIC_ORION.Sheets.Schedule);
  let pass = !isValidSelection(sheet.getActiveRange())
  let row = sheet.getActiveRange().getRow();
   /** Check to make sure selection meets validation criteria */
  if (!isValidSelection(sheet.getActiveRange())) {
    let errorMsg = isValidSelectionParams.filter(fn => !fn(sheet.getActiveRange()))
      .map(fn => fn.errorMsg)
    _Browser.msgBox(`${errorMsg.join('\\n')}`)
    
    return
  }
  // let hasScheduleTabFn = () => sheet.getParent().getSheetByName(STATIC_ORION.Sheets.Schedule.name)
  // hasScheduleTabFn.errorMsg = `Unable to find Sheet Name: ${STATIC_ORION.Sheets.Schedule.name}`
  const moveValuesToSchedule = copyValuesFromShortlist(sheet, shortListParams, _Browser)
  moveValuesToSchedule(scheduleSheet, scheduleParams);

  // pass && _Browser.msgBox(`The selected list have been appended to the "Schedule" Tab  \\n At the bottom`)

  function hasValidMoveCriterias(range) {
    return isValidSelection(range)
  }
}