const ORIONUPDATES = {
  Sheets: {
    PerksUSA: {
      id: "1iRNxGDH6Bj7Raf_d8mD-lp2sX6w2UMjYCgAzJN5xCiw",
      Names: {
        etc: 'etc',
      }
    },
    PerksSelfServiceTremendous: {
      id: "1XjEFkqQMJ61FZbMl0P982cPTYXa0Mfy6o-YUd-CiARI"
    },
    TremendousOrdering: {
      id: "1pFkK1mHurKE4wcp_uJYCFlCtwglVDNTN3A6f90YOo-Y"
    },
    PerksSelfService: {
      id: "181cE_4S9OA3Q1l8nUIu2qRZeN6L1eTaJekXWcPCxe-4"
    },
    Active: {
      getParticipantTrackerSheet: (spreadsheet) => spreadsheet.getSheetByName("Cap Checker") || spreadsheet.getSheetByName("Participant Tracker")
    }
  },
  Utils: {
    isBlankRow: (props) => Object.values(props).join("") === ""
  }
}

function testppc() {
  let trix = SpreadsheetApp.openById("1F7y9UOrITGRACpLdSfV_aN6wW4nuxdStExZvpuLTFPI");
  let _Browser = { msgBox: (msg) => Logger.log(msg) }
  let prt = trix.getSheetByName("Participant Tracker")
  trix.setActiveSheet(prt)
   capCheckerByEmail(trix, _Browser)
}

/*
 *
 * @param {undefined}
 * @return {Object : Array} populated with data from 2 SS as rowAsobjects
 */
function getPerksDataThisYear() {

  const getSpreadsheetById = (id) => {
    try{
      return SpreadsheetApp.openById(id)
      }catch(e){
        throw (`Unable to access spreadsheet ${id}. error: ${e}`)
      }
  }
  const ssPerksUSA = getSpreadsheetById(ORIONUPDATES.Sheets.PerksUSA.id)
  const ssPerksTremSelfService = getSpreadsheetById(ORIONUPDATES.Sheets.PerksSelfServiceTremendous.id)
  const ssPerksSelfService = getSpreadsheetById(ORIONUPDATES.Sheets.PerksSelfService.id);
  const ssTremendousOrdering = getSpreadsheetById(ORIONUPDATES.Sheets.TremendousOrdering.id);

  const unifyEmailAndCost = (emailHeader, amountHeader) => (props) => {
    let getStdAmount = (value) => isNaN(value) ? 0 : value 
    return Object.assign(props, { stdEmail: props[emailHeader].trim(), stdAmount: getStdAmount(Number(props[amountHeader])), })
    }
  const filterByUSHeaderName = (headerName) => ( props ) => {
    return (props.hasOwnProperty(headerName) && typeof props[headerName] === "string" )? props[headerName].trim() === "US": null
  }
  const hasProcessedCurrencyUSD = (props) => props?.["Processed Currency"] === "USD"
  const filterByUSCountry = filterByUSHeaderName("Country")

  const ssPerksSelfServiceData = getPerksSelfServiceData(ssPerksSelfService, { emailHeader: "Participant Email Address", valueHeader: "Code Value", sheetNameExt: "Codes" })
  const ssPerksTremSelfServiceData = getPerksSelfServiceData(ssPerksTremSelfService, { emailHeader: "Recipient Email Address", valueHeader: "Processed Amount", sheetNameExt: "" }, hasProcessedCurrencyUSD)
  const ssTremendousOrderingData = getPerksSelfServiceData(ssTremendousOrdering, { emailHeader: "Recipient Email Address", valueHeader: "Processed Amount", sheetNameExt: "Codes" }, hasProcessedCurrencyUSD)
  const ssPerksUSAData = getPerksUSAINT(ssPerksUSA)
  
  return [...ssPerksUSAData, ...ssPerksTremSelfServiceData, ...ssPerksSelfServiceData, ...ssTremendousOrderingData]
  /*
   * Pulls data for all tabs that have format of Q@/CurrentYear
   * @param {Object: Spreadsheet}
   * @return Array(props) an array of the row data as an object
   */
  function getPerksUSAINT(spreadsheet) {
    const fullYear = (new Date()).getFullYear()
    // regex doesn't work ?
    // const regexOfTab = new RegExp(`^q\\d\/${fullYear}`, "gi")
    const isQuartlySheetMatch = (sheet) => regexOfTab.test(sheet.getName())
    const sheets = spreadsheet.getSheets()
    const isfirstCharQ = (string) => typeof string === "string" ? string[0].toLowerCase() === 'q' : false
    const hasYearRegex = (string) => typeof string === "string" ? (new RegExp(fullYear, "gi").test(string)) : false
    const isValidSheet = (string) => (isfirstCharQ(string) && hasYearRegex(string))
    const filteredSheets = sheets.filter(sheet => isValidSheet(sheet.getName()))
    const isBlankRow = (props) => Object.values(props).join("") === ""
    try {
      const perksUSAData = filteredSheets.flatMap(sheet => {
        const { body } = getDataBySheet({ sheet, headerIdx: 0, bodyRowStart: 1 })
        return body
      }).filter(props => !isBlankRow(props))
        .map(unifyEmailAndCost("Email Address", "Code Value"))
      return perksUSAData
    } catch (e) {
      throw (`Unable to pull Perks USA Data error: ${e}`)
    }
  }
  function getPerksSelfServiceData(spreadsheet, { emailHeader, valueHeader, sheetNameExt }, filterCB = () => {return true}) {

    const fullYear = (new Date()).getFullYear()
    const regexOfTab = new RegExp(`^${fullYear} ${sheetNameExt}`, "gi")
    const isQuartlySheetMatch = (sheet) => regexOfTab.test(sheet.getName())
    const sheets = spreadsheet.getSheets().filter(isQuartlySheetMatch);
    const isBlankRow = (props) => Object.values(props).join("") === ""
    try {
      const perksSelfServiceData = sheets.flatMap(sheet => {
        const { body } = getDataBySheet({ sheet, headerIdx: 0, bodyRowStart: 1 })
        return body
      }).filter(props => !isBlankRow(props))
        .filter(filterCB)
        .map(unifyEmailAndCost(emailHeader, valueHeader))
      return perksSelfServiceData
    } catch (e) {
      throw (`Unable to pull ${spreadsheet.getName()} Data error: ${e}`)
    }
  }
}
function getCurrentQYYYY() {
  return `Q${getCurrentQuarter()}/${getFullYear()}`

  function getCurrentQuarter() {
    return getQuarter((new Date()).getMonth());

    function getQuarter(month) {
      let firstQuarter = month < 3;
      let secondQuarter = month > 2 && month < 6
      let thirdQuarter = month > 5 && month < 9
      let fourthQuarter = month > 8 && month < 12
      return firstQuarter ? 1 : secondQuarter ? 2 : thirdQuarter ? 3 : fourthQuarter ? 4 : null
    }
  }
  function getFullYear() {
    return (new Date()).getFullYear()
  }
}
function getParticipantTrackerData(spreadsheet) {
  const sheet = ORIONUPDATES.Sheets.Active.getParticipantTrackerSheet(spreadsheet);
  const { body } = getDataBySheet({ sheet, headerIdx: 3, bodyRowStart: 4 })
  return body.filter(({ "Email": email }) => email !== "")
}
function aggregateData(acc, props) {
  const { "stdEmail": email, "stdAmount": amount = 0 } = props
  const hasEmail = acc.find(props => props["stdEmail"] === email)
  const updateAmount = () => {
    hasEmail["stdAmount"] += isNaN(amount) ? 0 : Number(amount);
    return acc
  }
  if (isNaN(Number(amount))){
    debugger
  }
  const setAmount = () => acc.concat({ "stdEmail": email, "stdAmount": Number(amount) })
  return !!hasEmail ? updateAmount() : setAmount();
}
/*
 *
 * 
 */
function capCheckerByEmail(spreadsheet, _Browser) {
  spreadsheet = spreadsheet
  // _Browser = _Browser || Browser
  let sheetName = spreadsheet.getActiveSheet().getName()
  if ( !(sheetName === "Cap Checker" || sheetName === "Participant Tracker")) {
    _Browser.msgBox(`This action can only be dont on Cap Checker or Participant Tracker, not ${sheetName}`)
    return false
  }
  let perksDataThisYear = getPerksDataThisYear();
  let aggregatePerksDataThisYear = perksDataThisYear.reduce(aggregateData, [])
  // let byHi = aggregatePerksDataThisYear.sort( (a,b) => b.stdAmount - b.stdAmount).filter( ({stdEmail}) =>  /@google.com/.test(stdEmail))

  // const getEmail = props => props.hasOwnProperty["Email"] ? props["Email"] : ""
  let participantTrackerData = getParticipantTrackerData(spreadsheet)

  participantTrackerData.forEach(_checkPriceCap)
  debugger
  function _checkPriceCap(props) {
    const { "Email": email, getRangeByColHeader } = props
    const priceCap = /@google.com/.test(email) ? 65 : 600
    const perksCapRange = getRangeByColHeader("Perks Cap")
    const aggregateDataByEmail = aggregatePerksDataThisYear.find(({ stdEmail }) => stdEmail === email.trim());
    debugger
    const perksCapValue = !!aggregateDataByEmail ? aggregateDataByEmail.stdAmount : 0
    const amount = priceCap - perksCapValue
    if (isNaN(amount)){
      debugger
    }
    const yesNo = perksCapValue >= priceCap ? `(YES)` : `(NO)`
    const isPriceCapMeet = (conditional, range) => conditional ? setHightLight(range) : removeHighlight(range)
    const setHightLight = (range) => range.setFontColor("#ff0000").setBackground("#ffff00")
    const removeHighlight = (range) => range.setFontColor(null).setBackground(null)
    perksCapRange.setValue(`${perksCapValue} ${yesNo}`)
    isPriceCapMeet(perksCapValue >= priceCap, perksCapRange)
  }

}
