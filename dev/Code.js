
/*
 *
 * used by 
 * - moveToScheduleFromShortlist
 * - moveToShortListFromResponse
 */ 
const STATIC_ORION = {
  SS: {
    active: () => SpreadsheetApp.getActiveSheet()
  },
  Sheets: {
    ShortList: {
      name: "Shortlist",
      headerRowIdx: 4, // 0-based index
      dataStartRow: 5,
      defaultHeaders: [0, 1, 2, 3], 
      primaryHeadersAsString: ["Legal Name", "Email", "Phone Number", "Timezone"],
      primaryHeaders: [4, 5, 6, 7],
      defaultHeadersAsString: ["Participant Status", "Recruiter Notes", "Participant Review", "UXR Notes"],
      // defaultHeadersAsStr: ["Participant Status", "ICF", "Mark ineligible?*","Good Study Fit", "Detailed Feedback"], 
      // primaryHeadersAsStr: ["Legal Name",	"Email",	"Phone Number",	"Timezone"]
    },
    Schedule: {
      name: "Schedule",
      headerRowIdx: 3, // 0-based index
      dataStartRow: 4,
      defaultHeaders: [0, 1, 2, 3,4,5], 
      primaryHeaders: [7, 8, 9, 10]
    },
    "Participant Responses": {
      name: "Participant Responses",
      headerRowIdx: 0, // 0-based index
      dataStartRow: 1
    }
  },
  COLOR: {
    GREY: "#b7b7b7"
  },
  ConfirmAction: (titleMsg, columnHeader, _Browser) => (msg) => {
    const button = _Browser.msgBox(`${titleMsg} \\n`, _Browser.Buttons.YES_NO)
    // removing use of columnHeader for additional validation check
    // const button = _Browser.msgBox(`${titleMsg} \\n ${msg.map(props => props.hasOwnProperty(columnHeader) ? `Legal Name: "${props[columnHeader]}"` : props).filter(el => el !== "").join('\\n')}`, _Browser.Buttons.YES_NO)
    return button === 'yes'
  },
  ConfirmActionSchedule: (titleMsg, columnHeader, _Browser) => (msg) => {
    const button = _Browser.msgBox(`${titleMsg} \\n ${msg.map(props => props.hasOwnProperty('getRangeByColHeader') ? `row index: ${props.getRangeByColHeader("Timestamp").getRow()}` : props).filter(el => el !== "").join('\\n')}`, _Browser.Buttons.YES_NO)
    // const button = _Browser.msgBox(`${titleMsg} \\n ${msg.map(props => props.hasOwnProperty('getRangeByColHeader') ? `row index: ${props.getRangeByColHeader("Timestamp").getRow()}` : props).filter(el => el !== "").join('\\n')}`, _Browser.Buttons.YES_NO)
    return button === 'yes'
  }
}

function checkVersion(spreadsheet, _Browser){
  const version = "1.1.0"
  _Browser.msgBox(`Current Version: ${version}`)
}