function hasSS(){
  let ss = SpreadsheetApp.getActiveSpreadsheet()
  let url = ss.getUrl()
  debugger
}
function checkSignatures(spreadsheet, _Browser) {
  _Browser = _Browser || {msgBox: (str) => Logger.log(str)}
  // const confirmParticipantTrackerSheet = ""
  spreadsheet = spreadsheet || SpreadsheetApp.openById("1PHMuyib_uR0l5z6A9qj7M7OzomTnzz_0EnyHxrDNqOM")//.getSheetName("Participant Tracker")
  const sheetNames = spreadsheet.getSheets().map(prop => prop.getName())
  sheet = spreadsheet.getSheetByName("Participant Tracker")
  try {
    const { body } = getDataBySheet({ sheet, headerIdx: 3, bodyRowStart: 4 })
    const emptyNameAndOrEmail = (props) => ["Email", "Name"].some(str => props[str] !== "" )
    const searchGmail4ICFUpdateCell = compose(updateCellICF, getPermLink, getConversation);   
    const temp = body.filter(emptyNameAndOrEmail)
    
    let hasError = body.filter(emptyNameAndOrEmail)
                  .map((prop,idx) => searchGmail4ICFUpdateCell(prop, idx))
                  .reduce((acc,{errorMsg}) => errorMsg === undefined ? acc : acc.concat(`\\n errorMsg`), "")
                   debugger
  } catch (e) {
    if(hasError !== ""){
      throw(`Error: ${e}`)
    } 
  }
  debugger
  // const filterWithEmail
  function confirmSheet({ name, sheet, cb }) {
    return sheet.getName() === sheet ? true : cb()
  }

}

function searchGmail(searchTerm) {
  try{
    let response = GmailApp.search(searchTerm)
    debugger
    return response
  // return GmailApp.search(searchTerm)
} catch(e){
  return {errorMsg: `Unable to search GmailApp on ${e}`}
}
}

function getConversation({ "Email": emailAddress, "Name": fullName, getRangeByColHeader }, idx) {
  // emailAddress = "agnesjacob002@gmail.com"
  // fullname = "Aboko Agnes Unini"
  // const searchTerm = `( ${fullName} ${emailAddress} ) (echosign@echosign.com OR no-reply@docusign.com OR ux-mobile-tt@google.com)`;
  const nameAndEmail = `${fullName === "" ? emailAddress : fullName.concat(" ").concat(emailAddress)}`
  debugger
  const searchTerm = `( ${nameAndEmail} ) (echosign@echosign.com OR no-reply@docusign.com OR ux-mobile-tt@google.com)`;
  // debugger
  try {
    return Object.assign({}, {
      conversations: searchGmail(searchTerm),
      range: getRangeByColHeader("ICF"),
      searchTerm
    })
  } catch (e) {
    return { errorMsg: `Unable to fetch conversation: conversation:${searchGmail(searchTerm)}, range: ${getRangeByColHeader("ICF")} error: ${e}`, searchTerm }
  }
}
function setRichTextValueIFAVAIL({text, emailLink, range}){
  const buildRichText = () => SpreadsheetApp.newRichTextValue().setText(text).setLinkUrl(emailLink).build()
  debugger
  let hasLink = emailLink !== null
  let newRange = hasLink ? range.setRichTextValue(buildRichText()) : range.setValue(text).setHorizontalAlignment("left");
  return newRange
}
function getPermLink({ conversations, range, errorMsg, searchTerm }) {
  const text = conversations.length === 0 ? `Signature NOT FOUND, searchTerm: ${searchTerm}` : searchTerm
  try {
    return { text,
            emailLink: conversations.length === 0 ? null : conversations[0].getPermalink(), range }
  } catch (e) {
    return { text, errorMsg: !!errorMsg ? errorMsg : `Unable to fetch Permlink` }
  }
}
function updateCellICF({ text, range, emailLink, errorMsg }, idx, arr) {
  try {
    // range.setRichTextValue
    debugger
    let updatedRange = setRichTextValueIFAVAIL({text, emailLink, range})
    //  range.setValue(emailLink)
     return{ range: updatedRange, value: emailLink}
  } catch (e) {
    return { errorMsg: !!errorMsg ? errorMsg : `Unable to update Cell Values; error: ${e}` }
  }
}
